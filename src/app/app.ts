import {Component, View} from 'angular2/core';
import {Location, RouteConfig, RouterLink, Router, ROUTER_DIRECTIVES} from 'angular2/router';

import {Login} from '../login/login';
import {Signup} from '../signup/signup';
import {Home} from '../home/home';

@Component({
  selector: 'auth-app'
})
@View({
  directives: [ROUTER_DIRECTIVES],
  template: `
    <h1>Hello from app</h1>
    <router-outlet></router-outlet>
  `
})
@RouteConfig([
  { path: '/', redirectTo: ['/Login'] },
  { path: '/home', component: Home, as: 'Home' },
  { path: '/login', component: Login, as: 'Login' },
  { path: '/signup', component: Signup, as: 'Signup' }
])

export class App {
  constructor(public router: Router) {
  }
}

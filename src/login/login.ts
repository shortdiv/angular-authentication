import { Component, View, Injectable } from 'angular2/core';
import { AuthHttp, JwtHelper, tokenNotExpired } from 'angular2-jwt';
import { FORM_DIRECTIVES, NgIf } from 'angular2/common';

@Component({
  selector: 'login'
})
@View({
  directives: [ FORM_DIRECTIVES, NgIf ],
  templateUrl: 'src/login/login.html',
})

export class Login {

  lock = new Auth0Lock('YOUR_CLIENT_ID', 'YOUR_DOMAIN');

  constructor() {}

  login() {
    this.lock.show(function(err:string, profile:string, id_token:string) {

      if(err) {
        throw new Error(err);
      }

      localStorage.setItem('profile', JSON.stringify(profile));
      localStorage.setItem('id_token', id_token);

    });
  }

  logout() {
    localStorage.removeItem('profile');
    localStorage.removeItem('id_token');
  }

  loggedIn() {
    return tokenNotExpired();
  }

}

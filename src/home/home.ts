import { Component, View } from 'angular2/core';
import { CORE_DIRECTIVES } from 'angular2/common';
import { Router } from 'angular2/router';
import { tokenNotExpired } from 'angular2-jwt';

@Component({
  selector: 'home'
})
@View({
  directives: [CORE_DIRECTIVES],
  templateUrl: 'src/home/home.html'
})

export class Home {
  constructor(public router: Router) {
  }
}
